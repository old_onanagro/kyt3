﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class CarouselManager : MonoBehaviour
{
	CarouselItems[] m_carouesolItems;
	Vector3[] m_carouesolPositions;
	bool m_animating = false;
	int m_endAnim = 0;
	static CarouselManager g_instance;
	bool m_isMouseDown = false;
	[Tooltip ("Speed Multiplier for Drag ")]
	[SerializeField]
	int m_speed = 50;
	[Tooltip ("Minimum speed to reach next position ")]
	[SerializeField]
	int m_rollOverSpeed = 30;
	[Tooltip ("Maximum speed of each Card ")]
	[SerializeField]
	int m_maxSpeed = 100;
	[SerializeField]
	[Tooltip ("Minimum speed of Each Card")]
	int m_minSpeed = 20;
	[Tooltip ("Drag of cards' speed when moving")]
	[SerializeField]
	int m_drag = 10;
	[Tooltip ("Number of lerps to put the items perfectly on position")]
	[SerializeField]
	int m_lerpFraction = 10;
	//[SerializeField]
	bool m_circular = true;
	float m_time = 0;
	float m_animationSpeed = 0;

	Vector3 m_lastPosition = Vector3.zero;

	// Use this for initialization
	void Start ()
	{
		g_instance = this;
		m_carouesolItems = GetComponentsInChildren<CarouselItems> ();
		m_carouesolPositions = new Vector3[m_carouesolItems.Length];
		for (int i = 0; i < m_carouesolPositions.Length; i++) {
			m_carouesolPositions [i] = m_carouesolItems [i].transform.position;
			m_carouesolItems [i].ID = i;
		}

	}

	public int GetNextId (int a_id, int a_idDirection)
	{
		int nextId = m_carouesolItems [a_id].ID + a_idDirection;
		if (m_circular) {
			if (nextId >= m_carouesolItems.Length)
				nextId = 0;
			else if (nextId < 0)
				nextId = m_carouesolItems.Length - 1;
			return nextId;
		}
		return m_carouesolItems [a_id].ID;

	}

	public Vector3 GetNextPosition (int a_id, int a_idDirection)
	{
		int nextId = GetNextId (a_id, a_idDirection);
		return m_carouesolPositions [nextId];		
	}

	public Vector3 GetPosition (int a_id)
	{
		if (a_id < 0 || a_id >= m_carouesolPositions.Length)
			return Vector3.one;
		return m_carouesolPositions [a_id];
	}
	// Update is called once per frame
	void Update ()
	{
		m_time = (float)(Math.Truncate (100 * Time.deltaTime) / 100f);
		if (m_animating) {
			if (m_animationSpeed >= CarouselManager.INSTANCE.MIN_SPEED) {
				float drag = (float)(Math.Truncate (100 * CarouselManager.INSTANCE.DRAG * CarouselManager.INSTANCE.DELTA_TIME) / 100f);
				m_animationSpeed -= CarouselManager.INSTANCE.DRAG * CarouselManager.INSTANCE.DELTA_TIME;
				m_animationSpeed = (float)(Math.Truncate (100 * m_animationSpeed) / 100f);
			}

		}
		if (Input.GetMouseButtonDown (0))
			MouseDown (0, Input.mousePosition);
		else if (Input.GetMouseButtonUp (0))
			MouseUp (0, Input.mousePosition);
		else {
			if (Input.GetAxis ("Mouse X") != 0 || Input.GetAxis ("Mouse Y") != 0) {
				if (m_lastPosition.Equals (Vector3.zero)) {
					m_lastPosition = Input.mousePosition;
				} else {
					Vector3 delta = Input.mousePosition - m_lastPosition;
					MouseMove (delta, Input.mousePosition);

				}
			} else
				m_lastPosition = Vector3.zero;
		}
	
	}

	public void OnEndAnim (int a_baseId)
	{
		m_animating = false;
		int id = m_carouesolItems [a_baseId].ID;
		int currentId = id - a_baseId;
		if (currentId < 0) {
			currentId += 8;
		}
		int idDir = currentId;
		if (idDir > 0)
			idDir = 1;
		else
			idDir = -1;

		/*if (Mathf.Abs (idDir) > 1)
			idDir *= -1;*/
		if (currentId < 0) {
			currentId += 8;
		}
		for (int i = 0; i < m_carouesolItems.Length; i++) {
			m_carouesolItems [i].ID = i;
			for (int j = 0; j < currentId; j++) {
				m_carouesolItems [i].ID = GetNextId (i, idDir);
			}
			m_carouesolItems [i].OnEndAnim ();
		}
	}


	public void OnTouchDown (int a_id, Vector3 a_point)
	{
	}

	public void OnTouchUp (int a_id, Vector3 a_point)
	{
	}

	public void OnTouchDrag (int a_id, Vector3 a_delta, Vector3 a_position)
	{
	}

	public void MouseDown (int a_id, Vector3 a_point)
	{
		m_isMouseDown = true;
	}

	public void MouseUp (int a_id, Vector3 a_point)
	{
		m_isMouseDown = false;

		for (int i = 0; i < m_carouesolItems.Length; i++) {
			m_carouesolItems [i].LeaveAnimation ();
		}
		m_animating = true;
	}

	public void MouseMove (Vector3 a_delta, Vector3 a_position)
	{
		if (!m_isMouseDown)
			return;
		int id = 1;
		if (a_delta.x < 0)
			id = -1;
		//	m_animating = false;
		float speed = (a_delta.x * m_speed / (float)Screen.width);
		speed = (float)(Math.Truncate (100 * speed) / 100f);
		for (int i = 0; i < m_carouesolItems.Length; i++) {
			m_carouesolItems [i].Move (Mathf.Abs (speed), id);
		}
	}

	public bool ANIMATING {
		get {
			return m_animating;
		}
	}

	public static CarouselManager INSTANCE {
		get {
			return g_instance;
		}
	}

	public int MAX_SPEED {
		get {
			return m_maxSpeed;
		}
	}

	public int SPEED {
		get {
			return m_speed;
		}
	}

	public int DRAG {
		get {
			return m_drag;
		}
	}

	public int MIN_SPEED {
		get {
			return m_minSpeed;
		}
	}

	public int ROLL_OVER_SPEED {
		get {
			return m_rollOverSpeed;
		}
	}

	public float DELTA_TIME {
		get {
			return m_time;
		}
	}

	public float LERP_FRACTION {
		get {
			return m_lerpFraction;
		}
	}

	public float ANIMATION_SPEED {
		get {
			return m_animationSpeed;	
		}
		set {
			m_animationSpeed = value;
		}
	}
}
