﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class CarouselItems : MonoBehaviour
{

	int m_id = 0;
	int m_baseId = -1;
	int m_currentnxtId = -1;
	Vector3 m_nextPosition;
	float m_time = 0;
	float m_passedTime = 0;
	float m_lastSpeed = 0;
	// Use this for initialization
	void Start ()
	{
		
	}
	
	// Update is called once per frame
	void Update ()
	{
		if (!CarouselManager.INSTANCE.ANIMATING || m_currentnxtId == -1)
			return;

		Vector3 direction = m_nextPosition - transform.position; 
		float magnitude = direction.magnitude;
		direction.Normalize ();
		Vector3 delta = CarouselManager.INSTANCE.DELTA_TIME * CarouselManager.INSTANCE.ANIMATION_SPEED * direction;
		float ratio = 0;
		if (delta.magnitude > magnitude) {
			ratio = 1;
			transform.position = m_nextPosition;
		} else
			transform.position += delta;
		
		if (ratio == 1) {
			if (CarouselManager.INSTANCE.ANIMATION_SPEED < CarouselManager.INSTANCE.MIN_SPEED) {
				CarouselManager.INSTANCE.ANIMATION_SPEED = 0;
				m_id = m_currentnxtId;
				m_currentnxtId = -1;
				CarouselManager.INSTANCE.OnEndAnim (m_baseId);
				m_time = 0;
			} else {
				int dir = m_currentnxtId - m_id;
				if (Mathf.Abs (dir) > 1)
					dir *= -1;
				m_id = m_currentnxtId;
				if (dir > 0) {
					m_currentnxtId = CarouselManager.INSTANCE.GetNextId (m_baseId, 1);
					Vector3 nxtPos = CarouselManager.INSTANCE.GetPosition (m_currentnxtId);
					SetNextPosition (nxtPos, 0.5f);
				} else if (dir < 0) {
					m_currentnxtId = CarouselManager.INSTANCE.GetNextId (m_baseId, -1);
					Vector3 nxtPos = CarouselManager.INSTANCE.GetPosition (m_currentnxtId);
					SetNextPosition (nxtPos, 0.5f);
				} else {
					CarouselManager.INSTANCE.ANIMATION_SPEED = 0;
					m_currentnxtId = -1;
					CarouselManager.INSTANCE.OnEndAnim (m_baseId);
				}			

			}
		}
	}

	IEnumerator IterateToPosition ()
	{
		Vector3 pos = transform.position;
		float lerp = 0;
		float fraction = 1f / CarouselManager.INSTANCE.LERP_FRACTION;

		while (lerp < 1) {
			lerp += fraction;
			if (lerp > 1)
				lerp = 1;
			transform.position = Vector3.Lerp (pos, m_nextPosition, lerp);
			//pos = transform.position - m_nextPosition;
			yield return null;
		}
		transform.position = m_nextPosition;
		yield return null;
	}

	public void OnEndAnim ()
	{
		m_lastSpeed = 0;
		m_currentnxtId = -1;
		//Vector3 pos = transform.position;
		m_nextPosition = CarouselManager.INSTANCE.GetPosition (ID);
		StartCoroutine ("IterateToPosition");
	}

	public void SetNextPosition (Vector3 a_nextPosition, float a_time)
	{
		m_nextPosition = a_nextPosition;
		m_time = a_time;
		m_passedTime = 0;
	}

	public void LeaveAnimation ()
	{
		if (m_currentnxtId == -1 || CarouselManager.INSTANCE.ANIMATING)
			return;		
		Vector3 nxtPos = CarouselManager.INSTANCE.GetPosition (m_currentnxtId);
		Vector3 lastPos = CarouselManager.INSTANCE.GetPosition (m_id);
		float distanceNxt = (transform.position - nxtPos).magnitude;
		float distanceLast = (transform.position - lastPos).magnitude;
		if (distanceNxt > distanceLast && m_lastSpeed < CarouselManager.INSTANCE.ROLL_OVER_SPEED) {
			SetNextPosition (lastPos, 0.5f);
			m_currentnxtId = m_id;
			CarouselManager.INSTANCE.ANIMATION_SPEED = CarouselManager.INSTANCE.MIN_SPEED;
			//	m_lastSpeed =  CarouesolManager.INSTANCE.MIN_SPEED;
		} else {
			CarouselManager.INSTANCE.ANIMATION_SPEED = m_lastSpeed;
			SetNextPosition (nxtPos, 0.5f);
		}
	}

	public void Move (float a_speed, int a_idDirection)
	{
		
		if (CarouselManager.INSTANCE.ANIMATING && m_currentnxtId != -1) {
			int idDir = m_currentnxtId - m_id;
			if (Mathf.Abs (idDir) > 1)
				idDir *= -1;
			if ((a_idDirection < 0 && idDir < 0) || (a_idDirection > 0 && idDir > 0)) {
				if (CarouselManager.INSTANCE.ANIMATION_SPEED < CarouselManager.INSTANCE.MAX_SPEED)
					CarouselManager.INSTANCE.ANIMATION_SPEED += a_speed;
				return;
			} else {
				CarouselManager.INSTANCE.ANIMATION_SPEED -= a_speed;
				if (CarouselManager.INSTANCE.ANIMATION_SPEED < CarouselManager.INSTANCE.MIN_SPEED)
					CarouselManager.INSTANCE.ANIMATION_SPEED = CarouselManager.INSTANCE.MIN_SPEED;
				return;
			}
		}
		Vector3 nxtPos = CarouselManager.INSTANCE.GetNextPosition (m_baseId, a_idDirection);
		m_lastSpeed = a_speed;
		int nxtId = CarouselManager.INSTANCE.GetNextId (m_baseId, a_idDirection);
		if (m_currentnxtId == -1) {
			m_currentnxtId = nxtId;
		} else {
			if (m_currentnxtId != nxtId) {
				nxtPos = CarouselManager.INSTANCE.GetPosition (m_id);
			}
		}
		Vector3 dir = nxtPos - transform.position; 
		float magnitude = dir.magnitude;
		dir.Normalize ();
		Vector3 delta = CarouselManager.INSTANCE.DELTA_TIME * a_speed * dir;
		if (delta.magnitude > magnitude) {
			transform.position = nxtPos;
			m_currentnxtId = -1;
			m_lastSpeed = 0;
			if (nxtPos.Equals (CarouselManager.INSTANCE.GetNextPosition (m_baseId, a_idDirection))) {
				m_id = nxtId;
			}

		} else
			transform.position += delta;


	}

	public int ID {
		get {
			return m_id;
		}
		set {
			m_id = value;
			if (m_baseId == -1)
				m_baseId = value;
		}
	}
}
