﻿using System;
using UnityEngine;

/// <summary>
/// Path KeyPoint Event Object
/// </summary>
/// <remarks>
/// By GaraQuor - Oanangro 07/10/2017
/// </remarks>
public class Path_KeyPoint_Event : MonoBehaviour
{
    /// <summary>
    /// Event CallBack
    /// </summary>
    public virtual void Event(Path_KeyPoint_Event _event, Player _player)
    {

    }
}
