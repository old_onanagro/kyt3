﻿using MidiJack;
using System;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Utils Class for Note
/// </summary>
/// <remarks>
/// By GaraQuor - Onanagro 08/10/2017
/// </remarks>
public class UNote
{
    /// <summary>
    /// Note Dictionary
    /// </summary>
    public static Dictionary<string, int[]> NoteDictionary;

    /// <summary>
    /// Generate Dictionary
    /// </summary>
    public static void GenerateDictionary()
    {
        NoteDictionary = new Dictionary<string, int[]>();

        NoteDictionary.Add("C", new int[] { 0, 12, 24, 36, 48, 60, 72, 84, 96, 108, 120 });
        NoteDictionary.Add("C#", new int[] { 1, 13, 25, 37, 49, 61, 73, 85, 97, 109, 121 });
        NoteDictionary.Add("D", new int[] { 2, 14, 26, 38, 50, 62, 74, 86, 98, 110, 122 });
        NoteDictionary.Add("D#", new int[] { 3, 15, 27, 39, 51, 63, 75, 87, 99, 111, 123 });
        NoteDictionary.Add("E", new int[] { 4, 16, 28, 40, 52, 64, 76, 88, 100, 112, 124 });
        NoteDictionary.Add("F", new int[] { 5, 17, 29, 41, 53, 65, 77, 89, 101, 113, 125 });
        NoteDictionary.Add("F#", new int[] { 6, 18, 30, 42, 54, 66, 78, 90, 102, 114, 126 });
        NoteDictionary.Add("G", new int[] { 7, 19, 31, 43, 55, 67, 79, 91, 103, 115, 127 });
        NoteDictionary.Add("G#", new int[] { 8, 20, 32, 44, 56, 68, 80, 92, 104, 116 });
        NoteDictionary.Add("A", new int[] { 9, 21, 33, 45, 57, 69, 81, 93, 105, 117 });
        NoteDictionary.Add("A#", new int[] { 10, 22, 34, 46, 58, 70, 82, 94, 106, 118 });
        NoteDictionary.Add("B", new int[] { 11, 23, 35, 47, 59, 71, 83, 95, 107, 119 });
    }

    /// <summary>
    /// Note is Pressed
    /// </summary>
    public static bool IsPressed(string _note)
    {
        int _press_count = 0;
        bool _pressed = false;

        List<int> _values = new List<int>(NoteDictionary[_note]);

        for (int i = 0; i < 128; i++)
        {
            if(MidiMaster.GetKey(i) != 0)
            {
                _press_count++;

                if (_values.Contains(i))
                    _pressed = true;
            }
        }

        if (_note == "A" && Input.GetKey(KeyCode.A))
        {
            _press_count++;
            _pressed = true;
        }
        if (_note == "B" && Input.GetKey(KeyCode.B))
        {
            _press_count++;
            _pressed = true;
        }
        if (_note == "C" && Input.GetKey(KeyCode.C))
        {
            _press_count++;
            _pressed = true;
        }
        if (_note == "D" && Input.GetKey(KeyCode.D))
        {
            _press_count++;
            _pressed = true;
        }
        if (_note == "E" && Input.GetKey(KeyCode.E))
        {
            _press_count++;
            _pressed = true;
        }
        if (_note == "F" && Input.GetKey(KeyCode.F))
        {
            _press_count++;
            _pressed = true;
        }
        if (_note == "G" && Input.GetKey(KeyCode.G))
        {
            _press_count++;
            _pressed = true;
        }

        return _press_count == 1 && _pressed;
    }
}
