﻿using System;
using UnityEngine;

/// <summary>
/// Path Key Point Object
/// </summary>
/// <remarks>
/// By GaraQuor - Onangro 07/10/2017
/// </remarks>
[ExecuteInEditMode]
public class Path_KeyPoint : MonoBehaviour
{
    /// <summary>
    /// Is First
    /// </summary>
    public bool IsFirst;

    /// <summary>
    /// Next Key Point
    /// </summary>
    public Path_KeyPoint NextPoint;

    /// <summary>
    /// Player Speed at this point
    /// </summary>
    public float Speed = 2.0f;

    /// <summary>
    /// Wait Timer
    /// </summary>
    public float WaitDelay = 0.0f;

    /// <summary>
    /// Key Point Event
    /// </summary>
    public Path_KeyPoint_Event Event;

    /// <summary>
    /// Update Func Called from Unity
    /// </summary>
    private void Update()
    {

    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.cyan;
        Gizmos.DrawSphere(transform.position, 0.4f);

        if(NextPoint != null)
        {
            Gizmos.DrawLine(transform.position, NextPoint.transform.position);
        }
    }
}