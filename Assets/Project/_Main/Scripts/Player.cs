﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.AI;

/// <summary>
/// Player Class Object
/// </summary>
/// <remarks>
/// By GaraQuor - Onanagro 07/10/2017
/// </remarks>
public class Player : MonoBehaviour
{
    /// <summary>
    /// CurrentKeyPoint
    /// </summary>
    public Path_KeyPoint CurrentKeyPoint;

    /// <summary>
    /// Transform
    /// </summary>
    [HideInInspector]
    public Transform Transform;

    /// <summary>
    /// Nav Mesh Agent Component
    /// </summary>
    private NavMeshAgent NavAgent;

    /// <summary>
    /// Camera Transform
    /// </summary>
    private Transform CameraTransform;

    /// <summary>
    /// Initialize
    /// </summary>
    public void Init(Path_KeyPoint _firstkeypoint)
    {
        CurrentKeyPoint = _firstkeypoint;

        if (NavAgent == null)
            NavAgent = GetComponent<NavMeshAgent>();

        if (Transform == null)
            Transform = GetComponent<Transform>();

        if (CameraTransform == null)
            CameraTransform = Camera.main.GetComponent<Transform>();
    }
    
    /// <summary>
    /// Update Func Called from Unity
    /// </summary>
	private void Update ()
    {
        if(CurrentKeyPoint != null && CurrentKeyPoint.NextPoint != null)
        {
            NavAgent.destination = CurrentKeyPoint.NextPoint.transform.position;
            if ((transform.position - CurrentKeyPoint.NextPoint.transform.position).magnitude < 0.5f)
                CurrentKeyPoint = CurrentKeyPoint.NextPoint;
        }

        //Move Camera
        CameraTransform.position = new Vector3(Transform.position.x + 63.7f, Transform.position.y + 56.41f, Transform.position.z);
    }
}
