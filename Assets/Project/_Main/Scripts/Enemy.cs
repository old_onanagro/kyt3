﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

/// <summary>
/// Enemy Class Object
/// </summary>
/// <remarks>
/// By GaraQuor - Onanagro 07/10/2017
/// </remarks>
public class Enemy : MonoBehaviour
{
    /// <summary>
    /// Linked Note
    /// </summary>
    private Note LinkedNote;

    /// <summary>
    /// Is Initialized
    /// </summary>
    private bool Intialized;

    /// <summary>
    /// Navigation Path
    /// </summary>
    private NavMeshPath NavPath;

    /// <summary>
    /// Navigation Agent
    /// </summary>
    private NavMeshAgent NavAgent;

    /// <summary>
    /// Is Moving
    /// </summary>
    private bool Moving;

    /// <summary>
    /// Ref to Player
    /// </summary>
    private Player Player;

    /// <summary>
    /// Transform Component
    /// </summary>
    private Transform Transform;

    /// <summary>
    /// Initialize Object
    /// </summary>
    public void Init(Note _linked_note, Player _player)
    {
        LinkedNote = _linked_note;
        Player = _player;

        Transform = GetComponent<Transform>();
        NavAgent = GetComponent<NavMeshAgent>();

        NavPath = new NavMeshPath();

        Moving = true;
        Intialized = true;

        NavAgent.CalculatePath(Player.Transform.position, NavPath);
        NavAgent.isStopped = true;
    }

    /// <summary>
    /// Update Func Called from Unity
    /// </summary>
    private void Update ()
    {
		if(Intialized)
        {
            NavAgent.CalculatePath(Player.Transform.position, NavPath);
            NavAgent.isStopped = true;

            if (Moving)
            {
                float _lenght = GetPathLenght(NavPath);
                Vector3 _direction = GetPathDirection(NavPath);
                float _delta_time = LinkedNote.PlayTimer - Time.time + 0.8f;

                if (_delta_time > 0.5f)
                {
                    float _speed = _lenght / _delta_time;
                    Transform.position = new Vector3(Transform.position.x, Transform.position.y, Transform.position.z) + (_direction * _speed * Time.deltaTime);

                    float _angle = Mathf.Atan2(_direction.x, _direction.z ) * Mathf.Rad2Deg;
                    Transform.eulerAngles = new Vector3(Transform.eulerAngles.x, _angle, Transform.eulerAngles.z);
                }
                else
                {
                    Moving = false;
                    Destroy(gameObject);
                }
            }
        }
	}

    /// <summary>
    /// Get Path Lenght
    /// </summary>
    private float GetPathLenght(NavMeshPath _path)
    {
        if (_path.corners.Length > 1)
        {
            float _lenght = 0;
            for (int i = 0; i < _path.corners.Length - 1; i++)
                _lenght += (_path.corners[i + 1] - _path.corners[i]).magnitude;

            return _lenght;
        }

        return 0;
    }

    /// <summary>
    /// Get Path Direction
    /// </summary>
    private Vector3 GetPathDirection(NavMeshPath _path)
    {
        if (_path.corners.Length > 1)
            return (_path.corners[1] - _path.corners[0]).normalized;

        return Vector3.zero;
    }

    private void OnDrawGizmos()
    {
        if (NavPath != null)
        {
            Gizmos.color = Color.green;

            for (int i = 0; i < NavPath.corners.Length; i++)
            {
                Gizmos.DrawSphere(NavPath.corners[i], 0.5f);

                if (i > 0)
                    Gizmos.DrawLine(NavPath.corners[i - 1], NavPath.corners[i]);
            }
        }
    }
}
