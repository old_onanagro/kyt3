﻿using System;
using UnityEngine;

public class Center_UI_Shake : MonoBehaviour
{
    public static Center_UI_Shake Instance;

    // Transform of the camera to shake. Grabs the gameObject's transform
    // if null.
    private Transform camTransform;

    // How long the object should shake for.
    private float shakeDuration = 0f;

    // Amplitude of the shake. A larger value shakes the camera harder.
    private float shakeAmount = 0.7f;
    private float decreaseFactor = 1.0f;

    private Vector3 originalPos;

    private SpriteRenderer Renderer;

    public void Shake(float _duration, float _shakeamount = 0.7f, float _decreaseFactor = 1.0f)
    {
        shakeDuration = _duration;
        shakeAmount = _shakeamount;
        decreaseFactor = _decreaseFactor;
    }

    void Awake()
    {
        if (camTransform == null)
        {
            camTransform = GetComponent<Transform>();
            Instance = this;
        }

        if (Renderer == null)
            Renderer = GetComponent<SpriteRenderer>();
    }

    void OnEnable()
    {
        originalPos = camTransform.localPosition;
    }

    void Update()
    {
        if (shakeDuration > 0)
        {
            camTransform.localPosition = originalPos + UnityEngine.Random.insideUnitSphere * shakeAmount;
            shakeDuration -= Time.deltaTime * decreaseFactor;

            Renderer.color = Color.Lerp(Color.white, Color.red, shakeDuration * 2.0f);
        }
        else
        {
            shakeDuration = 0f;
            camTransform.localPosition = originalPos;
            Renderer.color = Color.white;
        }
    }
}