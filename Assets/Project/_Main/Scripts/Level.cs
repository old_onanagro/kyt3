﻿using SonicBloom.Koreo.Players;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Level Object
/// </summary>
/// <remarks>
/// By GaraQuor - Onanagro 07/10/2017
/// </remarks>
public class Level : MonoBehaviour
{
    /// <summary>
    /// Level Id
    /// </summary>
    public int Id;

    /// <summary>
    /// Start Delay
    /// </summary>
    public float StartDelay = 5;

    /// <summary>
    /// Player Prefab
    /// </summary>
    public Player PlayerPrefab;

    /// <summary>
    /// text Mesh Score
    /// </summary>
    public TextMesh TextScore;

    /// <summary>
    /// text Mesh Score
    /// </summary>
    public TextMesh TextScore_Shadow;

    /// <summary>
    /// Note Manager
    /// </summary>
    public Note_Manager NoteManager;

    /// <summary>
    /// Enemy Manager
    /// </summary>
    public Enemy_Manager EnemyManager;

    /// <summary>
    /// Player Instance
    /// </summary>
    private Player Player;

    /// <summary>
    /// Start Music Flag
    /// </summary>
    private bool StartedMusicFlag;

    /// <summary>
    /// Starte Music Timer
    /// </summary>
    private float StartedMusicTimer;

    /// <summary>
    /// Start Func called from Unity
    /// </summary>
	private void Start ()
    {
        Instance = this;

        Path_KeyPoint _first_keypoint = GetFirstKeyPoint();

        Player = Instantiate(PlayerPrefab);
        Player.transform.parent = transform;
        Player.transform.position = _first_keypoint.transform.position;
        Player.Init(_first_keypoint);

        EnemyManager.Init(Player);
        NoteManager.Init(EnemyManager);

        StartedMusicTimer = Time.time + StartDelay;
    }

    private Path_KeyPoint GetFirstKeyPoint()
    {
        Path_KeyPoint[] _keypoints = GetComponentsInChildren<Path_KeyPoint>();

        for (int i = 0; i < _keypoints.Length; i++)
        {
            if (_keypoints[i].IsFirst)
                return _keypoints[i];
        }

        return null;
    }

    /// <summary>
    /// Update Func Called from Unity
    /// </summary>
    private void Update ()
    {
        NoteManager.Update();
        EnemyManager.Update();

        UpdateStartMusic();

        if (Input.GetKey(KeyCode.Escape))
            UnityEngine.SceneManagement.SceneManager.LoadScene(0);
    }

    /// <summary>
    /// Update Start Music
    /// </summary>
    private void UpdateStartMusic()
    {
        if (!StartedMusicFlag && Time.time > StartedMusicTimer)
        {
            NoteManager.StartPlay();
            StartedMusicFlag = true;
        }
    }

    private int Score;

    private static Level Instance;

    public static void AddScore(int _score)
    {
        if(Instance != null)
        {
            Instance.Score += _score;

            Instance.TextScore.text = "SCORE: " + Instance.Score;
            Instance.TextScore_Shadow.text = "SCORE: " + Instance.Score;
        }
    }
}
