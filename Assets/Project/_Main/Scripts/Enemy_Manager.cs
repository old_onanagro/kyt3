﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.AI;

/// <summary>
/// Enemy Manager
/// </summary>
/// <remarks>
/// By GaraQuor - Onanagro 07/10/2017
/// </remarks>
[Serializable]
public class Enemy_Manager
{
    /// <summary>
    /// Enemy Prefab
    /// </summary>
    public Enemy EnemyPrefab;

    /// <summary>
    /// Enemy Pool Transform
    /// </summary>
    public Transform EnemyPoolTransform;

    /// <summary>
    /// Enemy List
    /// </summary>
    private List<Enemy> EnemyList;

    /// <summary>
    /// Player Ref
    /// </summary>
    private Player Player;

    /// <summary>
    /// Is Initialized
    /// </summary>
    private bool Initialized;

    /// <summary>
    /// Init Manager
    /// </summary>
    public void Init(Player _player)
    {
        Player = _player;
        EnemyList = new List<Enemy>();

        Initialized = true;
    }

    /// <summary>
    /// Update Func Called from Unity
    /// </summary>
    public void Update()
    {
        if(Initialized)
        {
            for (int i = 0; i < EnemyList.Count;)
            {
                if (EnemyList[i] == null)
                    EnemyList.RemoveAt(i);
                else
                    i++;
            }
        }
    }

    /// <summary>
    /// Pop a Enenmy
    /// </summary>
    public void PopEnemy(Note _note)
    {
        if(EnemyList.Count < 3)
        {
            Enemy _enemy = GameObject.Instantiate(EnemyPrefab);

            _enemy.transform.parent = EnemyPoolTransform;
            _enemy.transform.position = RandomPosition(Player.Transform.position, 10.0f, 20.0f, 20);
            _enemy.Init(_note, Player);

            EnemyList.Add(_enemy);
        }
    }

    /// <summary>
    /// Get Random Position in NavMesh
    /// </summary>
    private Vector3 RandomPosition(Vector3 _position, float _radius, float _max_radius, int _delta_count)
    {
        float _angle = UnityEngine.Random.Range(0.0f, Mathf.PI * 2.0f);
        Vector3 _pos = _position;

        for (int i = 0; i < _delta_count; i++)
        {
            NavMeshHit _hit;

            float _delta_angle = _angle + Mathf.PI * 2 / _delta_count * i;

            NavMesh.SamplePosition(_position + new Vector3(Mathf.Cos(_delta_angle) * _radius, Mathf.Sin(_delta_angle) * _radius, 0.0f), out _hit, _max_radius, 1);

            if (_hit.hit && (_hit.position - _position).magnitude > (_pos - _position).magnitude)
                _pos = _hit.position;
        }

        return _pos;
    }
}
