﻿using MidiJack;
using SonicBloom.Koreo;
using System;
using System.Collections;
using UnityEngine;

/// <summary>
/// Note Mono Object
/// </summary>
/// <remarks>
/// By GaraQuor - Onanagro 07/10/2017
/// </remarks>
public class Note_Node : MonoBehaviour
{
    /// <summary>
    /// Note Text
    /// </summary>
    public string NoteText;

    /// <summary>
    /// Linked Note
    /// </summary>
    public Note LinkedNote;

    /// <summary>
    /// Koregragphy
    /// </summary>
    private Koreography Koregraphy;
    
    /// <summary>
    /// Transform Component
    /// </summary>
    private Transform Transform;

    /// <summary>
    /// Sprite Renderer Component
    /// </summary>
    private SpriteRenderer Renderer;

    /// <summary>
    /// Start Func Called from Unity
    /// </summary>
	public void Init(Note _linked_note, Koreography _koregraphy)
    {
        LinkedNote = _linked_note;
        Koregraphy = _koregraphy;

        Transform = GetComponent<Transform>();
        Renderer = GetComponent<SpriteRenderer>();
    }

    private bool LastState;

    private bool Fail;

    /// <summary>
    /// Update Func Called from Unity
    /// </summary>
	private void Update ()
    {
        float _deltatime = LinkedNote.PlayTimer - Time.time + 0.8f;
        bool _pressed = UNote.IsPressed(LinkedNote.NoteText);

        if (!LinkedNote.Checked)
        {
            //Can Press
            if(_deltatime < 0.75f)
            {
                if (_deltatime > 0.4f)
                {
                    Transform.localScale = new Vector3(0.7f, 0.7f, 1.0f);
                    Renderer.color = new Color(Renderer.color.r, Renderer.color.g, Renderer.color.b, 1.0f);

                    if (_pressed && !LastState)
                    {
                        Destroy(gameObject);
                        Level.AddScore(25);
                    }
                }
                else
                {
                    Fail = true;
                    LinkedNote.Checked = true;

                    Center_UI_Shake.Instance.Shake(0.5f, 0.2f, 1);
                }
            }
            
            //Moving
            if (_deltatime > 0.3f)
            {
                float _lenght = Transform.localPosition.magnitude;
                float _speed = _lenght / _deltatime;

                Transform.localPosition = Transform.localPosition + (Transform.localPosition.normalized * -_speed * Time.deltaTime);
            }

            LastState = _pressed;
        }
        else
        {
            if(Fail)
            {
                if (_deltatime > 0)
                {
                    Renderer.color = new Color(1, 0, 0, _deltatime);
                }
                else
                    Destroy(gameObject);
            }
        }
    }

    private int GetSampleTime()
    {
        return Koregraphy.GetLatestSampleTime();
    }
}

/// <summary>
/// Note object ? LOL
/// </summary>
/// <remarks>
/// By GaraQuor - Onanagro 07/10/2017
/// </remarks>
public class Note
{
    /// <summary>
    /// Note Play Timer
    /// </summary>
    public float PlayTimer;

    /// <summary>
    /// Start Sample
    /// </summary>
    public int StartSample;

    /// <summary>
    /// Note Text
    /// </summary>
    public string NoteText;

    /// <summary>
    /// Checked
    /// </summary>
    public bool Added;

    /// <summary>
    /// Checked
    /// </summary>
    public bool Checked;

    /// <summary>
    /// Constructor
    /// </summary>
    public Note(float _play_timer, int _start_sample, string _note_text)
    {
        PlayTimer = _play_timer;
        StartSample = _start_sample;
        NoteText = _note_text;

        Added = false;
        Checked = false;
    }
}