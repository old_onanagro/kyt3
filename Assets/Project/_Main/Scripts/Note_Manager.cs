﻿using SonicBloom.Koreo;
using SonicBloom.Koreo.Players;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Note Manager
/// </summary>
/// <remarks>
/// By GaraQuor - BattleKart 07/10/2017
/// </remarks>
[Serializable]
public class Note_Manager
{
    /// <summary>
    /// Parent Playing AudioSource
    /// </summary>
    [HideInInspector]
    public bool Playing;

    /// <summary>
    /// Pre plop Time
    /// </summary>
    public float PrePlopTime = 2.0f;

    /// <summary>
    /// Enemy prefab
    /// </summary>
    public List<Note_Node> NotePrefabs;

    /// <summary>
    /// Note Node Pool
    /// </summary>
    public Transform NoteNode_Pool;

    /// <summary>
    /// Koregrapher Simple Music Player
    /// </summary>
    public SimpleMusicPlayer MusicPlayer;

    /// <summary>
    /// Is Initialized
    /// </summary>
    private bool Initialized;

    /// <summary>
    /// Current Koregraphy
    /// </summary>
    private Koreography Koregraphy;

    /// <summary>
    /// Ref to local enemy Manager
    /// </summary>
    private Enemy_Manager EnenmyManager;

    /// <summary>
    /// Note List
    /// </summary>
    private List<Note> NoteList;

    /// <summary>
    /// Note Node List
    /// </summary>
    private List<Note_Node> NoteNodeList;

    /// <summary>
    /// Initialize Manager
    /// </summary>
    public void Init(Enemy_Manager _enemy_manager)
    {
        EnenmyManager = _enemy_manager;

        NoteNodeList = new List<Note_Node>();
        UNote.GenerateDictionary();

        Playing = false;
        Initialized = true;
    }

    /// <summary>
    /// Start Playing and compute Note PlayTimer
    /// </summary>
    public void StartPlay()
    {
        MusicPlayer.Play();

        Koregraphy = Koreographer.Instance.GetKoreographyAtIndex(0);

        KoreographyTrackBase _koregraphy_track = Koregraphy.GetTrackByID("NoteTrack");
        List<KoreographyEvent> _events_list = _koregraphy_track.GetAllEvents();

        int _sample_time = GetSampleTime();

        NoteList = new List<Note>();
        for (int i = 0; i < _events_list.Count; i++)
        {
            float _play_timer = Time.time + (float)(_events_list[i].StartSample - _sample_time) / (float)Koregraphy.SampleRate;
            NoteList.Add(new Note(_play_timer - 0.2f, _events_list[i].StartSample, _events_list[i].GetTextValue().Trim()));
        }

        Playing = true;
    }

    /// <summary>
    /// Update Func Called from Unity
    /// </summary>
	public void Update()
    {
        if (Initialized && Playing)
        {
            for (int i = 3; i < NoteList.Count; i++)
            {
                if(!NoteList[i].Added && Time.time > NoteList[i].PlayTimer - PrePlopTime)
                {
                    PopNoteNode(NoteList[i]);
                    EnenmyManager.PopEnemy(NoteList[i]);

                    NoteList[i].Added = true;
                }
            }
        }
	}

    /// <summary>
    /// Pop a NoteNode
    /// </summary>
    private void PopNoteNode(Note _note)
    {
        Note_Node _node = GameObject.Instantiate(GetNoteNodePrefab(_note.NoteText));
        _node.transform.parent = NoteNode_Pool.transform;

        float _angle = UnityEngine.Random.Range(0.0f, Mathf.PI * 2.0f);
        _node.transform.localPosition = new Vector3(Mathf.Cos(_angle) * 15.0f, Mathf.Sin(_angle) * 15.0f, 0.0f);
        _node.transform.localScale = new Vector3(0.45f, 0.45f, 1.0f);
        _node.transform.localEulerAngles = Vector3.zero;

        _node.Init(_note, Koregraphy);

        NoteNodeList.Add(_node);
    }

    private Note_Node GetNoteNodePrefab(string _note)
    {
        for (int i = 0; i < NotePrefabs.Count; i++)
        {
            if (NotePrefabs[i].NoteText == _note)
                return NotePrefabs[i];
        }

        Debug.Log("Fail found:'" + _note + "'");

        return null;
    }

    private int GetSampleTime()
    {
        return Koregraphy.GetLatestSampleTime();
    }
}
