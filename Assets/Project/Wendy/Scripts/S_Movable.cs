﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class S_Movable : MonoBehaviour {

    public Vector3 m_speed;

    public Transform m_target;

	// Use this for initialization
	void Start () {
        m_transform = GetComponent<Transform>();
	}
	
	// Update is called once per frame
	void Update () {
        m_transform.Translate( m_speed, m_target );
	}

    private Transform m_transform;
}
