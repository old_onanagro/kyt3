﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class S_openDoor : MonoBehaviour
{

    public LayerMask[] m_layer;
    public float m_speed = 1f;

    // Use this for initialization
    void Start()
    {
        m_mesh = GetComponent<SkinnedMeshRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        if (m_colliding)
        {
            m_lerp += Time.deltaTime * m_speed;
            if (m_lerp > 1)
            {
                m_lerp = 1;
            }


            m_mesh.SetBlendShapeWeight( 0, Mathf.Lerp( 0, 100, m_lerp ) );

        }
        else
        {

            m_lerp -= Time.deltaTime * m_speed;

            if (m_lerp < 0)
            {
                m_lerp = 0;
            }
            float temp = 1 - m_lerp;

            m_mesh.SetBlendShapeWeight( 0, Mathf.Lerp( 100, 0, temp ) );


        }
    }



    private void OnTriggerEnter(Collider other)
    {
        if (m_layer[ 0 ] == ( m_layer[ 0 ] | ( 1 << other.gameObject.layer ) ) /* || m_layer[ 1 ] == ( m_layer[ 1 ] | ( 1 << other.gameObject.layer )) */)
        {
            m_colliding = true;

        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (m_layer[ 0 ] == ( m_layer[ 0 ] | ( 1 << other.gameObject.layer ) ) /* || m_layer[ 1 ] == ( m_layer[ 1 ] | ( 1 << other.gameObject.layer )) */)
        {
            m_colliding = false;

        }
    }


    private float m_lerp = 0;
    private SkinnedMeshRenderer m_mesh;
    private bool m_colliding;
}
