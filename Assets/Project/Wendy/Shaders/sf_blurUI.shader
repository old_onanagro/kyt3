// Shader created with Shader Forge v1.38 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.38;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,cgin:,lico:1,lgpr:1,limd:1,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,imps:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:0,bsrc:3,bdst:7,dpts:2,wrdp:False,dith:0,atcv:False,rfrpo:True,rfrpn:Refraction,coma:15,ufog:True,aust:True,igpj:True,qofs:0,qpre:3,rntp:2,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,atwp:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False,fsmp:False;n:type:ShaderForge.SFN_Final,id:4013,x:32992,y:32700,varname:node_4013,prsc:2|alpha-5948-U,refract-4667-OUT;n:type:ShaderForge.SFN_TexCoord,id:1729,x:32242,y:32879,varname:node_1729,prsc:2,uv:0,uaff:False;n:type:ShaderForge.SFN_Panner,id:6051,x:32448,y:32879,varname:node_6051,prsc:2,spu:0.01,spv:0.01|UVIN-1729-UVOUT;n:type:ShaderForge.SFN_TexCoord,id:5948,x:32260,y:33075,varname:node_5948,prsc:2,uv:0,uaff:False;n:type:ShaderForge.SFN_Multiply,id:4667,x:32604,y:33041,varname:node_4667,prsc:2|A-6051-UVOUT,B-5948-UVOUT;n:type:ShaderForge.SFN_Multiply,id:9322,x:32819,y:32822,varname:node_9322,prsc:2|A-4667-OUT,B-2912-OUT;n:type:ShaderForge.SFN_Multiply,id:2912,x:32773,y:33054,varname:node_2912,prsc:2|A-4667-OUT,B-1038-OUT;n:type:ShaderForge.SFN_Vector1,id:1038,x:32547,y:33204,varname:node_1038,prsc:2,v1:-1;pass:END;sub:END;*/

Shader "Shader Forge/sf_blurUI" {
    Properties {
        [HideInInspector]_Cutoff ("Alpha cutoff", Range(0,1)) = 0.5
    }
    SubShader {
        Tags {
            "IgnoreProjector"="True"
            "Queue"="Transparent"
            "RenderType"="Transparent"
        }
        GrabPass{ }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            Blend SrcAlpha OneMinusSrcAlpha
            ZWrite Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase
            #pragma multi_compile_fog
            #pragma only_renderers d3d9 d3d11 glcore gles 
            #pragma target 3.0
            uniform sampler2D _GrabTexture;
            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 projPos : TEXCOORD1;
                UNITY_FOG_COORDS(2)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.pos = UnityObjectToClipPos( v.vertex );
                UNITY_TRANSFER_FOG(o,o.pos);
                o.projPos = ComputeScreenPos (o.pos);
                COMPUTE_EYEDEPTH(o.projPos.z);
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                float4 node_9532 = _Time;
                float2 node_6051 = (i.uv0+node_9532.g*float2(0.01,0.01));
                float2 node_4667 = (node_6051*i.uv0);
                float2 sceneUVs = (i.projPos.xy / i.projPos.w) + node_4667;
                float4 sceneColor = tex2D(_GrabTexture, sceneUVs);
////// Lighting:
                float3 finalColor = 0;
                fixed4 finalRGBA = fixed4(lerp(sceneColor.rgb, finalColor,i.uv0.r),1);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
